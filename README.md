# Плейбук для развертывания тестового приложения NodeJs (Задание #240106).

* Ansible Playbook для установки нужных сервисов и мониторинга (docker, haproxy) находится в папке Ansible. Все роли подключаемые роли разрабатываются в отдельных репозиториях, покрыты тестами Molecule. Установка осуществляется из файла requirements.yml посредством ansible-galaxy. Реализована установка через GitLab CI/CD Pipeline.

* Пример запуска тестов
```
molecule test
```

* Пример использования Ansible Playbook
```
- ansible-galaxy install -r requirements.yml
- ansible-playbook playbook.yml -i ./inventory/Dev/hosts
```
* Пример настроек для работы playbook
```
# Playbook Haproxy settings
_haproxy_target_port: 80
_haproxy_target_group: 'game'
_haproxy_source_group: 'lb'
# Playbook Docker game container settings
_game_docker_image: yeasy/simple-web
_game_docker_tag: latest
_game_docker_container_port: 80
_game_service_name: 'game2048'
# Role Haproxy list of backend servers.
haproxy_backend_servers: "{{ groups[_haproxy_target_group]
            | map('extract',hostvars) 
            | items2dict(key_name='inventory_hostname', value_name='ansible_host') 
            | dict2items(key_name='name', value_name='address') 
            | map('combine', {'port': _haproxy_target_port}) 
            }}"
```

* Пример playbook
```
---
- hosts: game
  roles:
    - sample.docker
  become: true
  post_tasks:
    - block:
      - name: Game | Create service unit
        ansible.builtin.template:
          src: templates/docker_container.j2
          dest: "/etc/systemd/system/{{ _game_service_name }}.service"
        notify: Restart game
      - name: Game | Flush handlers if needed
        meta: flush_handlers
      - name: Game | Start Game service
        ansible.builtin.systemd:
          name: '{{ _game_service_name }}'
          daemon_reload: true
          state: started
      - name: Game | Open game ports with ufw
        community.general.ufw:
          port: '{{ _haproxy_target_port }}'
          proto: tcp
          rule: allow
          src: '{{ item }}'
        loop: "{{ groups[_haproxy_source_group] | map('extract', hostvars, 'ansible_host') }}"
      - name: Game | Open ssh ports with ufw
        community.general.ufw:
          rule: allow
          name: OpenSSH
      - name: Game | enable ufw
        community.general.ufw:
          state: enabled
      tags: notest
  become: true  
  handlers:
    - name: Restart game
      ansible.builtin.systemd: 
        name: "{{ _game_service_name }}"
        daemon_reload: true
        state: restarted
  become: true 

- hosts: lb
  roles:
    - sample.haproxy
  become: true
  post_tasks:
    - block:
      - name: LB | Open frontend Haproxy ports with ufw
        community.general.ufw:
          port: '80'
          proto: tcp
          rule: allow
      - name: LB | Open ssh ports with ufw
        community.general.ufw:
          rule: allow
          name: OpenSSH
      - name: LB | enable ufw
        community.general.ufw:
          state: enabled
      tags: notest
  become: true  
```

License
-------

GPLv3

Author Information
------------------

* Kirill Fedorov (tg:fedrr) as a challege for ClassBook #240106 project https://deusops.com/classbook